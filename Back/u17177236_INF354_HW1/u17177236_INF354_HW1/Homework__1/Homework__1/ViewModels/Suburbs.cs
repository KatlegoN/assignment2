﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Homework__1.ViewModels
{
    public class Suburb
    {
        public int Suburb_ID { get; set; }
        public string Suburb_Name { get; set; }
        public int Province_ID { get; set; }
    }
}