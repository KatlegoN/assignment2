﻿using System;
using System.Collections.Generic;
using Homework__1.Models;
using Homework__1.ViewModels;
using System.Dynamic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Data.SqlClient;

namespace Homework__1.Controllers
{
    public class ProvinceController: ApiController
    {
        
        [System.Web.Http.Route("api/Province/getAllProvinces")]
        [System.Web.Mvc.HttpPost]

        public List<dynamic> getAllProvinces()
        {
            South_AfricaEntities db = new South_AfricaEntities();
            db.Configuration.ProxyCreationEnabled = false;
            return getProvincesReturnList(db.Provinces.ToList());
        }

        private List<dynamic> getProvincesReturnList(List<Province> province)
        {
            List<dynamic> myProvinces = new List<dynamic>();
            foreach (Province p in province)
            {
                dynamic myProvince = new ExpandoObject();
                myProvince.ID = p.Province_ID;
                myProvince.Name = p.Province_Name;
                myProvinces.Add(myProvince);
               
            }
            return myProvinces;
        }

        [System.Web.Http.Route("api/Province/getAllProvincesWithCities")]
        [System.Web.Mvc.HttpPost]
        public List<dynamic> getAllProvincesWithCities()
        {
            South_AfricaEntities db = new South_AfricaEntities();
            db.Configuration.ProxyCreationEnabled = false;
            List<Province> provinces = db.Provinces.Include(xx => xx.Cities).ToList();
            return getAllProvincesWithCities(provinces);
           
        }



        private List<dynamic> getAllProvincesWithCities(List<Province> provinces)
        {
            List<dynamic> myProvinces = new List<dynamic>();
            foreach (Province p in provinces)
            {
                dynamic nPronvinces = new ExpandoObject();
                nPronvinces.ID = p.Province_ID;
                nPronvinces.Name = p.Province_Name;
                nPronvinces.Cities = getCities(p);
                myProvinces.Add(nPronvinces);
            }
            return myProvinces;
           
         }
         private List<dynamic> getCities(Province province)
        {
            List<dynamic> myCities = new List<dynamic>();
            foreach(City c in province.Cities)
            {
                dynamic myCity = new ExpandoObject();
                myCity.ID = c.City_ID;
                myCity.Name = c.City_Name;
                myCities.Add(myCity);
            }
            return myCities;
        }

        [System.Web.Http.Route("api/Province/addProvincesWithCities")]
        [System.Web.Mvc.HttpPost]
        public List<dynamic> addProvincesWithCities([FromBody] List<Province> province)
        {
            if (province != null)
            {
                South_AfricaEntities db = new South_AfricaEntities();
                db.Configuration.ProxyCreationEnabled = false;
                db.Provinces.AddRange(province);
                db.SaveChanges();
                return getAllProvincesWithCities();
            }

            else
            {
                return null;
            }
        }

    }

   
}